from django.contrib.auth import get_user_model
from django.db import models

User = get_user_model()


class MedicamentGroup(models.Model):
    title = models.CharField(max_length=256)


class Medicament(models.Model):
    title = models.CharField(max_length=256)
    cost = models.IntegerField()
    urls = models.CharField(max_length=4096)
    group = models.ForeignKey(MedicamentGroup, on_delete=models.CASCADE)
